namespace CM.WinRT.Demos.Messages
{
    public class ChatMessage
    {
        public ChatMessage(string from, string message)
        {
            From = from;
            Message = message;
        }

        public string From { get; set; }
        public string Message { get; set; }
    }
}