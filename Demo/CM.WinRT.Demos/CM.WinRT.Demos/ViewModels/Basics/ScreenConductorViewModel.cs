using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class ScreenConductorViewModel : Conductor<Screen>.Collection.OneActiveWithNavigation
    {
        private readonly INavigationService _navigationService;

        public ScreenConductorViewModel(INavigationService navigationService) : base(navigationService)
        {
            _navigationService = navigationService;

            DisplayName = "Screens, Conductors & Screen Lifecycle";

            Items.Add(new ScreenSubViewModel { DisplayName = "Item 1" });
            Items.Add(new ScreenSubViewModel { DisplayName = "Item 2" });
            Items.Add(new ScreenSubViewModel { DisplayName = "Item 3" });
            Items.Add(new ScreenSubViewModel { DisplayName = "Item 4" });
            Items.Add(new ScreenSubViewModel { DisplayName = "Item 5" });
            ActivateItem(Items[0]);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
        }

        public override void CanClose(System.Action<bool> callback)
        {
            base.CanClose(callback);
        }
    }
}