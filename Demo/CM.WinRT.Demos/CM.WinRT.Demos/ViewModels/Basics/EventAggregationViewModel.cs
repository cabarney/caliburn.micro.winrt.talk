using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class EventAggregationViewModel : ScreenWithNavigation
    {
        private EventsViewModel _chatter1;
        private EventsViewModel _chatter2;

        public EventAggregationViewModel(INavigationService navigation, IEventAggregator events) : base(navigation)
        {
            DisplayName = "Event Aggregation / Messaging";

            Chatter1 = new EventsViewModel(events) { DisplayName="Left"};
            Chatter2 = new EventsViewModel(events) {DisplayName="Right"};
        }

        public EventsViewModel Chatter1
        {
            get { return _chatter1; }
            set
            {
                if (Equals(value, _chatter1)) return;
                _chatter1 = value;
                NotifyOfPropertyChange(() => Chatter1);
            }
        }

        public EventsViewModel Chatter2
        {
            get { return _chatter2; }
            set
            {
                if (Equals(value, _chatter2)) return;
                _chatter2 = value;
                NotifyOfPropertyChange(() => Chatter2);
            }
        }
    }
}