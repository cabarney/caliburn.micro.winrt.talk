using System.Collections.Generic;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras.Results;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class CoroutinesViewModel : ScreenWithNavigation
    {
        public CoroutinesViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Coroutines";
        }

        public IEnumerable<IResult> Execute()
        {
            yield return new VisualStateResult("Loading");
            yield return new DelayResult(3000);  // Do some important work
            yield return new VisualStateResult("Normal");
            yield return new MessageDialogResult("The work is finished!", "Done");
        }
    }
}
