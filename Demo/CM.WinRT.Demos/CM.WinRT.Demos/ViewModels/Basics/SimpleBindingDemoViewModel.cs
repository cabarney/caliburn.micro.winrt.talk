using System.Collections.Generic;
using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class SimpleBindingDemoViewModel : ScreenWithNavigation
    {
        private string _firstName;
        private PersonViewModel _albert;

        public SimpleBindingDemoViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Simple Binding";


            FirstName = "Joe";
            Albert = new PersonViewModel {FirstName = "Albert", LastName = "Einstein"};
            FirstNames = new List<string>
                        {
                            "Frank",
                            "Bob",
                            "Joe",
                            "Steve",
                            "Gunther"
                        };
            

            People = new BindableCollection<PersonViewModel>
                         {
                             new PersonViewModel{FirstName="Albert", LastName="Einstein"},
                             new PersonViewModel{FirstName="Johannes", LastName="Kepler"},
                             new PersonViewModel{FirstName="Isaac", LastName="Newton"},
                             new PersonViewModel{FirstName="Erwin", LastName="Schroedinger"},
                         };
        }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value == _firstName) return;
                _firstName = value;
                NotifyOfPropertyChange(() => FirstName);
            }
        }

        public PersonViewModel Albert
        {
            get { return _albert; }
            set
            {
                if (Equals(value, _albert)) return;
                _albert = value;
                NotifyOfPropertyChange(() => Albert);
            }
        }

        public List<string> FirstNames { get; set; }

        public BindableCollection<PersonViewModel> People { get; set; }
    }

    public class PersonViewModel : PropertyChangedBase
    {
        private string _firstName;
        private string _lastName;

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value == _firstName) return;
                _firstName = value;
                NotifyOfPropertyChange(() => FirstName);
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (value == _lastName) return;
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
            }
        }
    }
}
