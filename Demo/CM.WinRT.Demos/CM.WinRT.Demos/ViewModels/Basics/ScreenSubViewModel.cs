using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class ScreenSubViewModel : Screen
    {
        protected override void OnActivate()
        {
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
        }

        public override void CanClose(System.Action<bool> callback)
        {
            base.CanClose(callback);
        }
    }
}