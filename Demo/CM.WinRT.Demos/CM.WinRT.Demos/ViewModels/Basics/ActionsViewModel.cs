using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class ActionsViewModel : ScreenWithNavigation
    {
        private string _message;
        private string _someone;
        private string _helloMessage;
        private string _hoverMessage;

        public ActionsViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Actions";
        }

        public void SaySomething()
        {
            Message = "Caliburn.Micro Actions are better than Commands!";
        }

        public string Message
        {
            get { return _message; }
            set
            {
                if (value == _message) return;
                _message = value;
                NotifyOfPropertyChange(() => Message);
            }
        }

        public string Someone
        {
            get { return _someone; }
            set
            {
                if (value == _someone) return;
                _someone = value;
                NotifyOfPropertyChange(() => Someone);
                NotifyOfPropertyChange(() => CanSayHello);
            }
        }

        public string HelloMessage
        {
            get { return _helloMessage; }
            set
            {
                if (value == _helloMessage) return;
                _helloMessage = value;
                NotifyOfPropertyChange(() => HelloMessage);
            }
        }

        public void SayHello()
        {
            HelloMessage = "Hello, " + Someone + "!!!";
        }

        public void SayHelloWithParameter(string someone)
        {
            HelloMessage = "Hello, " + someone + "!!!";
        }

        public bool CanSayHello { get { return !string.IsNullOrEmpty(Someone); } }


        public string HoverMessage
        {
            get { return _hoverMessage; }
            set
            {
                if (value == _hoverMessage) return;
                _hoverMessage = value;
                NotifyOfPropertyChange(() => HoverMessage);
            }
        }

        public void ToggleHoverMessage()
        {
            if (string.IsNullOrEmpty(HoverMessage))
                HoverMessage = "Get off my lawn!";
            else
                HoverMessage = string.Empty;
        }
    }
}
