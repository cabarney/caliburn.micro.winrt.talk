using CM.WinRT.Demos.Messages;
using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Basics
{
    public class EventsViewModel : Screen, IHandle<ChatMessage>
    {
        private readonly IEventAggregator _events;
        private string _message;

        public EventsViewModel(IEventAggregator events)
        {
            _events = events;
            _events.Subscribe(this);
            Messages = new BindableCollection<ChatMessage>();
        }

        public BindableCollection<ChatMessage> Messages { get; set; }

        public void Handle(ChatMessage message)
        {
            Messages.Add(message);
        }

        public string Message
        {
            get { return _message; }
            set
            {
                if (value == _message) return;
                _message = value;
                NotifyOfPropertyChange(() => Message);
            }
        }

        public void Send()
        {
            _events.Publish(new ChatMessage(DisplayName, Message));
            Message = "";
        }
    }
}
