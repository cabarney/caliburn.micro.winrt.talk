using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels
{
    public class DemoGroup
    {
        public string Title { get; set; }
        public BindableCollection<Demo> Demos { get; set; }
    }
}