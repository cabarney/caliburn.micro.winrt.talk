using System.Collections.Generic;
using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels
{
    public class Conductor<T>
    {
        public class Collection
        {
            public class AllActiveWithNavigation : Caliburn.Micro.Conductor<T>.Collection.AllActive, INavigationViewModel
            {
                protected readonly INavigationService Navigation;

                protected AllActiveWithNavigation(INavigationService navigation)
                {
                    Navigation = navigation;
                }

                public virtual void GoBack()
                {
                    Navigation.GoBack();
                }

                public virtual bool CanGoBack
                {
                    get { return Navigation.CanGoBack; }
                }
            }

            public class OneActiveWithNavigation : Caliburn.Micro.Conductor<T>.Collection.OneActive, INavigationViewModel
            {
                protected readonly INavigationService Navigation;

                protected OneActiveWithNavigation(INavigationService navigation)
                {
                    Navigation = navigation;
                }

                public virtual void GoBack()
                {
                    Navigation.GoBack();
                }

                public virtual bool CanGoBack
                {
                    get { return Navigation.CanGoBack; }
                }
            }
        }
    }
}