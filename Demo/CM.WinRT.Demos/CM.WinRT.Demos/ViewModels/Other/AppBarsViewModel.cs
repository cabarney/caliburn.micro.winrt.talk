﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras;
using Caliburn.Micro.WinRT.Extras.Results;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace CM.WinRT.Demos.ViewModels.Other
{
    public class AppBarsViewModel : ScreenWithNavigation
    {
        private readonly IFlyoutHelper _flyoutHelper;
        private bool _appBarsOpen;

        public AppBarsViewModel(INavigationService navigation, IFlyoutHelper flyoutHelper) : base(navigation)
        {
            _flyoutHelper = flyoutHelper;
            DisplayName = "Using App Bars";
        }

        public void ShowAppBars()
        {
            AppBarsOpen = true;
        }

        public void ShowFlyout(UIElement sender)
        {
            _flyoutHelper.ShowDialog<FlyoutInfoViewModel>(PlacementMode.Top, sender, vm=>vm.Message="App Bar Flyout!", null);
        }

        public bool AppBarsOpen
        {
            get { return _appBarsOpen; }
            set
            {
                if (value.Equals(_appBarsOpen)) return;
                _appBarsOpen = value;
                NotifyOfPropertyChange(() => AppBarsOpen);
            }
        }
    }
}
