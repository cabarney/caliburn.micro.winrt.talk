﻿using System.Collections.Generic;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras.Results;

namespace CM.WinRT.Demos.ViewModels.Other
{
    public class ViewStateViewModel : ScreenWithNavigation
    {
        public ViewStateViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Working with View States";
        }

        public bool IsBig { get; set; }

        public IEnumerable<IResult> SwitchState()
        {
            if (IsBig)
                yield return new VisualStateResult("Normal");
            else
                yield return new VisualStateResult("Big");
            IsBig = !IsBig;
        }
    }
}