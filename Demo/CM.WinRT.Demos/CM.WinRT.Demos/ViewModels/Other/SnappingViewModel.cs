﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras.Results;
using Windows.UI.ViewManagement;

namespace CM.WinRT.Demos.ViewModels.Other
{
    public class SnappingViewModel : ScreenWithNavigation
    {
        public SnappingViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Responding to Orientation and Snapping";
            FillData();
        }

        public TeamDataSource TeamData { get; set; }

        public IEnumerable<IResult> SizeChanged()
        {
            yield return new VisualStateResult(ApplicationView.Value.ToString(), true);
        }

        private void FillData()
        {
            TeamData = new TeamDataSource(new List<Team>
                                              {
                                                  new Team("Air Force", "Falcons", "Colorado Springs", "Colorado",
                                                           "Mountain West"),
                                                  new Team("Akron", "Zips", "Akron", "Ohio", "MAC"),
                                                  new Team("Alabama", "Crimson Tide", "Tuscaloosa", "Alabama", "SEC"),
                                                  new Team("Arizona", "Wildcats", "Tucson", "Arizona", "Pac-12"),
                                                  new Team("Arizona State", "Sun Devils", "Tempe", "Arizona", "Pac-12"),
                                                  new Team("Arkansas", "Razorbacks", "Fayetteville", "Arkansas", "SEC"),
                                                  new Team("Arkansas State", "Red Wolves", "Jonesboro", "Arkansas",
                                                           "Sun Belt"),
                                                  new Team("Army", "Black Knights", "West Point", "New York",
                                                           "Independent"),
                                                  new Team("Auburn", "Tigers", "Auburn", "Alabama", "SEC"),
                                                  new Team("Ball State", "Cardinals", "Muncie", "Indiana", "MAC"),
                                                  new Team("Baylor", "Bears", "Waco", "Texas", "Big 12"),
                                                  new Team("Boise State", "Broncos", "Boise", "Idaho", "Mountain West"),
                                                  new Team("Boston College", "Eagles", "Chestnut Hill", "Massachusetts",
                                                           "ACC"),
                                                  new Team("Bowling Green", "Falcons", "Bowling Green", "Ohio", "MAC"),
                                                  new Team("Buffalo", "Bulls", "Buffalo", "New York", "MAC"),
                                                  new Team("BYU", "Cougars", "Provo", "Utah", "Independent"),
                                                  new Team("California", "Golden Bears", "Berkeley", "California",
                                                           "Pac-12"),
                                                  new Team("Central Michigan", "Chippewas", "Mount Pleasant", "Michigan",
                                                           "MAC"),
                                                  new Team("Charlotte", "49ers", "Charlotte", "North Carolina", "C-USA"),
                                                  new Team("Cincinnati", "Bearcats", "Cincinnati", "Ohio", "Big East"),
                                                  new Team("Clemson", "Tigers", "Clemson", "South Carolina", "ACC"),
                                                  new Team("Colorado", "Buffaloes", "Boulder", "Colorado", "Pac-12"),
                                                  new Team("Colorado State", "Rams", "Fort Collins", "Colorado",
                                                           "Mountain West"),
                                                  new Team("Connecticut", "Huskies", "Storrs", "Connecticut", "Big East"),
                                                  new Team("Duke", "Blue Devils", "Durham", "North Carolina", "ACC"),
                                                  new Team("Eastern Michigan", "Eagles", "Ypsilanti", "Michigan", "MAC"),
                                                  new Team("East Carolina", "Pirates", "Greenville", "North Carolina",
                                                           "C-USA"),
                                                  new Team("Florida", "Gators", "Gainesville", "Florida", "SEC"),
                                                  new Team("Florida Atlantic", "Owls", "Boca Raton", "Florida",
                                                           "Sun Belt"),
                                                  new Team("FIU", "Golden Panthers", "Miami", "Florida", "Sun Belt"),
                                                  new Team("Florida State", "Seminoles", "Tallahassee", "Florida", "ACC"),
                                                  new Team("Fresno State", "Bulldogs", "Fresno", "California",
                                                           "Mountain West"),
                                                  new Team("Georgia", "Bulldogs", "Athens", "Georgia", "SEC"),
                                                  new Team("Georgia Tech", "Yellow Jackets", "Atlanta", "Georgia", "ACC"),
                                                  new Team("Georgia State", "Panthers", "Atlanta", "Georgia", "Sun Belt"),
                                                  new Team("Hawai?i", "Warriors", "Honolulu", "Hawaii", "Mountain West"),
                                                  new Team("Houston", "Cougars", "Houston", "Texas", "C-USA"),
                                                  new Team("Idaho", "Vandals", "Moscow", "Idaho", "WAC"),
                                                  new Team("Illinois", "Fighting Illini", "Urbana-Champaign", "Illinois",
                                                           "Big Ten"),
                                                  new Team("Indiana", "Hoosiers", "Bloomington", "Indiana", "Big Ten"),
                                                  new Team("Iowa", "Hawkeyes", "Iowa City", "Iowa", "Big Ten"),
                                                  new Team("Iowa State", "Cyclones", "Ames", "Iowa", "Big 12"),
                                                  new Team("Kansas", "Jayhawks", "Lawrence", "Kansas", "Big 12"),
                                                  new Team("Kansas State", "Wildcats", "Manhattan", "Kansas", "Big 12"),
                                                  new Team("Kent State", "Golden Flashes", "Kent", "Ohio", "MAC"),
                                                  new Team("Kentucky", "Wildcats", "Lexington", "Kentucky", "SEC"),
                                                  new Team("Louisiana Tech", "Bulldogs", "Ruston", "Louisiana", "WAC"),
                                                  new Team("Louisiana-Lafayette", "Ragin' Cajuns", "Lafayette",
                                                           "Louisiana", "Sun Belt"),
                                                  new Team("Louisiana-Monroe", "Warhawks", "Monroe", "Louisiana",
                                                           "Sun Belt"),
                                                  new Team("Louisville", "Cardinals", "Louisville", "Kentucky",
                                                           "Big East"),
                                                  new Team("LSU", "Tigers", "Baton Rouge", "Louisiana", "SEC"),
                                                  new Team("Marshall", "Thundering Herd", "Huntington", "West Virginia",
                                                           "C-USA"),
                                                  new Team("Maryland", "Terrapins", "College Park", "Maryland", "ACC"),
                                                  new Team("Massachusetts", "Minutemen", "Amherst", "Massachusetts",
                                                           "MAC"),
                                                  new Team("Memphis", "Tigers", "Memphis", "Tennessee", "C-USA"),
                                                  new Team("Miami", "Hurricanes", "Coral Gables", "Florida", "ACC"),
                                                  new Team("Miami University", "RedHawks", "Oxford", "Ohio", "MAC"),
                                                  new Team("Michigan", "Wolverines", "Ann Arbor", "Michigan", "Big Ten"),
                                                  new Team("Michigan State", "Spartans", "East Lansing", "Michigan",
                                                           "Big Ten"),
                                                  new Team("Middle Tennessee", "Blue Raiders", "Murfreesboro",
                                                           "Tennessee", "Sun Belt"),
                                                  new Team("Minnesota", "Golden Gophers", "Minneapolis", "Minnesota",
                                                           "Big Ten"),
                                                  new Team("Mississippi State", "Bulldogs", "Starkville", "Mississippi",
                                                           "SEC"),
                                                  new Team("Missouri", "Tigers", "Columbia", "Missouri", "SEC"),
                                                  new Team("Navy", "Midshipmen", "Annapolis", "Maryland", "Independent"),
                                                  new Team("NC State", "Wolfpack", "Raleigh", "North Carolina", "ACC"),
                                                  new Team("Nebraska", "Cornhuskers", "Lincoln", "Nebraska", "Big Ten"),
                                                  new Team("Nevada", "Wolf Pack", "Reno", "Nevada", "Mountain West"),
                                                  new Team("New Mexico", "Lobos", "Albuquerque", "New Mexico",
                                                           "Mountain West"),
                                                  new Team("New Mexico State", "Aggies", "Las Cruces", "New Mexico",
                                                           "WAC"),
                                                  new Team("North Carolina", "Tar Heels", "Chapel Hill",
                                                           "North Carolina", "ACC"),
                                                  new Team("North Texas", "Mean Green", "Denton", "Texas", "Sun Belt"),
                                                  new Team("NIU", "Huskies", "DeKalb", "Illinois", "MAC"),
                                                  new Team("Northwestern", "Wildcats", "Evanston", "Illinois", "Big Ten"),
                                                  new Team("Notre Dame", "Fighting Irish", "South Bend", "Indiana",
                                                           "Independent"),
                                                  new Team("Ohio", "Bobcats", "Athens", "Ohio", "MAC"),
                                                  new Team("Ohio State", "Buckeyes", "Columbus", "Ohio", "Big Ten"),
                                                  new Team("Oklahoma", "Sooners", "Norman", "Oklahoma", "Big 12"),
                                                  new Team("Oklahoma State", "Cowboys", "Stillwater", "Oklahoma",
                                                           "Big 12"),
                                                  new Team("Old Dominion", "Monarchs", "Norfolk", "Virginia", "C-USA"),
                                                  new Team("Ole Miss", "Rebels", "Oxford", "Mississippi", "SEC"),
                                                  new Team("Oregon", "Ducks", "Eugene", "Oregon", "Pac-12"),
                                                  new Team("Oregon State", "Beavers", "Corvallis", "Oregon", "Pac-12"),
                                                  new Team("Penn State", "Nittany Lions", "University Park",
                                                           "Pennsylvania", "Big Ten"),
                                                  new Team("Pittsburgh", "Panthers", "Pittsburgh", "Pennsylvania",
                                                           "Big East"),
                                                  new Team("Purdue", "Boilermakers", "West Lafayette", "Indiana",
                                                           "Big Ten"),
                                                  new Team("Rice", "Owls", "Houston", "Texas", "C-USA"),
                                                  new Team("Rutgers", "Scarlet Knights", "Piscataway", "New Jersey",
                                                           "Big East"),
                                                  new Team("San Diego State", "Aztecs", "San Diego", "California",
                                                           "Mountain West"),
                                                  new Team("San Jose State", "Spartans", "San Jose", "California", "WAC"),
                                                  new Team("SMU", "Mustangs", "University Park", "Texas", "C-USA"),
                                                  new Team("South Alabama", "Jaguars", "Mobile", "Alabama", "Sun Belt"),
                                                  new Team("South Carolina", "Gamecocks", "Columbia", "South Carolina",
                                                           "SEC"),
                                                  new Team("South Florida", "Bulls", "Tampa", "Florida", "Big East"),
                                                  new Team("Southern Miss", "Golden Eagles", "Hattiesburg",
                                                           "Mississippi", "C-USA"),
                                                  new Team("Stanford", "Cardinal", "Stanford", "California", "Pac-12"),
                                                  new Team("Syracuse", "Orange", "Syracuse", "New York", "Big East"),
                                                  new Team("TCU", "Horned Frogs", "Fort Worth", "Texas", "Big 12"),
                                                  new Team("Temple", "Owls", "Philadelphia", "Pennsylvania", "Big East"),
                                                  new Team("Tennessee", "Volunteers", "Knoxville", "Tennessee", "SEC"),
                                                  new Team("Texas", "Longhorns", "Austin", "Texas", "Big 12"),
                                                  new Team("Texas A&M", "Aggies", "College Station", "Texas", "SEC"),
                                                  new Team("Texas State", "Bobcats", "San Marcos", "Texas", "WAC"),
                                                  new Team("Texas Tech", "Red Raiders", "Lubbock", "Texas", "Big 12"),
                                                  new Team("Toledo", "Rockets", "Toledo", "Ohio", "MAC"),
                                                  new Team("Troy", "Trojans", "Troy", "Alabama", "Sun Belt"),
                                                  new Team("Tulane", "Green Wave", "New Orleans", "Louisiana", "C-USA"),
                                                  new Team("Tulsa", "Golden Hurricane", "Tulsa", "Oklahoma", "C-USA"),
                                                  new Team("UAB", "Blazers", "Birmingham", "Alabama", "C-USA"),
                                                  new Team("UCF", "Knights", "Orlando", "Florida", "C-USA"),
                                                  new Team("UCLA", "Bruins", "Los Angeles", "California", "Pac-12"),
                                                  new Team("UNLV", "Rebels", "Las Vegas", "Nevada", "Mountain West"),
                                                  new Team("USC", "Trojans", "Los Angeles", "California", "Pac-12"),
                                                  new Team("Utah", "Utes", "Salt Lake City", "Utah", "Pac-12"),
                                                  new Team("Utah State", "Aggies", "Logan", "Utah", "WAC"),
                                                  new Team("UTEP", "Miners", "El Paso", "Texas", "C-USA"),
                                                  new Team("UTSA", "Roadrunners", "San Antonio", "Texas", "WAC"),
                                                  new Team("UIW", "Cardinals", "San Antonio", "Texas", "SLC"),
                                                  new Team("Vanderbilt", "Commodores", "Nashville", "Tennessee", "SEC"),
                                                  new Team("Virginia", "Cavaliers", "Charlottesville", "Virginia", "ACC"),
                                                  new Team("Virginia Tech", "Hokies", "Blacksburg", "Virginia", "ACC"),
                                                  new Team("Wake Forest", "Demon Deacons", "Winston-Salem",
                                                           "North Carolina", "ACC"),
                                                  new Team("Washington", "Huskies", "Seattle", "Washington", "Pac-12"),
                                                  new Team("Washington State", "Cougars", "Pullman", "Washington",
                                                           "Pac-12"),
                                                  new Team("West Virginia", "Mountaineers", "Morgantown",
                                                           "West Virginia", "Big 12"),
                                                  new Team("Western Kentucky", "Hilltoppers", "Bowling Green",
                                                           "Kentucky", "Sun Belt"),
                                                  new Team("Western Michigan", "Broncos", "Kalamazoo", "Michigan", "MAC"),
                                                  new Team("Wisconsin", "Badgers", "Madison", "Wisconsin", "Big Ten"),
                                                  new Team("Wyoming", "Cowboys", "Laramie", "Wyoming", "Mountain West")
                                              });
        }
    }

    public class TeamDataSource
    {
        public TeamDataSource(IEnumerable<Team> teams)
        {
            _teams = teams;
        }

        private readonly IEnumerable<Team> _teams;

        public IEnumerable<Conference> Conferences { get
        {
            return _teams.Select(x => x.Conference).Distinct().Select(
                    x => new Conference {Title = x, Teams = _teams.Where(t => t.Conference == x)}).OrderBy(x => x.Title);
        }}
    }

    public class Conference
    {
        public string Title { get; set; }
        public IEnumerable<Team> Teams { get; set; }
    }

    public class Team
    {
        public Team(string school, string mascot, string city, string state, string conference)
        {
            Title = school;
            Subtitle = mascot;
            Conference = conference;
            Description = city + ", " + state;
        }

        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Conference { get; set; }
        public string Description { get; set; }
    }
}
