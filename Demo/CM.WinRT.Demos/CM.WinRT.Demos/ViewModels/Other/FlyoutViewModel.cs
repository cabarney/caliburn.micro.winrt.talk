﻿using System.Collections.Generic;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras;
using Caliburn.Micro.WinRT.Extras.Results;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace CM.WinRT.Demos.ViewModels.Other
{
    public class FlyoutViewModel : ScreenWithNavigation
    {
        private readonly IFlyoutHelper _flyoutHelper;
        private string _textFromFlyout;

        public FlyoutViewModel(INavigationService navigation, IFlyoutHelper flyoutHelper) : base(navigation)
        {
            _flyoutHelper = flyoutHelper;
            DisplayName = "Flyouts";
        }

        public void ShowFlyout(UIElement element)
        {
            _flyoutHelper.ShowDialog<FlyoutInfoViewModel>(PlacementMode.Top, element, vm=>vm.Message="A message in a flyout!", vm=>TextFromFlyout=vm.EditableText); 
        }

        public string TextFromFlyout
        {
            get { return _textFromFlyout; }
            set
            {
                if (value == _textFromFlyout) return;
                _textFromFlyout = value;
                NotifyOfPropertyChange(() => TextFromFlyout);
            }
        }
    }

    public class FlyoutInfoViewModel : Screen
    {
        private string _message;
        private string _editableText;

        public string Message
        {
            get { return _message; }
            set
            {
                if (value == _message) return;
                _message = value;
                NotifyOfPropertyChange(() => Message);
            }
        }

        public string EditableText
        {
            get { return _editableText; }
            set
            {
                if (value == _editableText) return;
                _editableText = value;
                NotifyOfPropertyChange(() => EditableText);
            }
        }
    }
}
