﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras.Results;

namespace CM.WinRT.Demos.ViewModels.Other
{
    public class MessageBoxViewModel : ScreenWithNavigation
    {
        private string _resultMessage;

        public MessageBoxViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Message Boxes";
        }

        public IEnumerable<IResult> SimpleMessageBox()
        {
            yield return new MessageDialogResult("This is a message for your user", "Important FYI!");
        }

        public IEnumerable<IResult> MoreComplex()
        {
            // This is probably NOT the ideal way to do this!  More work needs to be done
            // around dialogs in Caliburn.Micro.WinRT.  Alternatively, work is being done
            // in Callisto to aid in showing interactive dialogs - once this is published,
            // we can write a nice wrapper around it for use in Caliburn.Micro.

            var cmds = new List<MessageDialogCommand>
                           {
                               new MessageDialogCommand("Obama", x => DisplayResult("You voted for Obama")),
                               new MessageDialogCommand("Romney", x => DisplayResult("You voted for Romney")),
                               new MessageDialogCommand("The lesser of two evils", x => DisplayResult("Ugh."))
                           };
            yield return new MessageDialogResult("Who are you voting for in the upcoming presidential election?", "Cast your vote!", cmds);
        }

        public void DisplayResult(string answer)
        {
            ResultMessage = "Thank you for voting!  " + answer;
        }

        public string ResultMessage
        {
            get { return _resultMessage; }
            set
            {
                if (value == _resultMessage) return;
                _resultMessage = value;
                NotifyOfPropertyChange(() => ResultMessage);
            }
        }
    }
}
