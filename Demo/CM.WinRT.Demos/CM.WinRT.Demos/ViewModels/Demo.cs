﻿using System;

namespace CM.WinRT.Demos.ViewModels
{
    public class Demo
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public Type ViewModelType { get; set; }
    }
}