namespace CM.WinRT.Demos.ViewModels
{
    public interface INavigationViewModel
    {
        void GoBack();
        bool CanGoBack { get; }
    }
}