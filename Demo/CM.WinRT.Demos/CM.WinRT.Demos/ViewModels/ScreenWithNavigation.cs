using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels
{
    public abstract class ScreenWithNavigation : Screen, INavigationViewModel
    {
        protected readonly INavigationService Navigation;

        protected ScreenWithNavigation(INavigationService navigation)
        {
            Navigation = navigation;
        }

        public virtual void GoBack()
        {
            Navigation.GoBack();
        }

        public virtual bool CanGoBack { get { return Navigation.CanGoBack; } }
    }
}