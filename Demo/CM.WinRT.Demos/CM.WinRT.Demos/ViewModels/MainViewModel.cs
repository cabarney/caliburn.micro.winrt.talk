﻿using CM.WinRT.Demos.ViewModels.Annoyances;
using CM.WinRT.Demos.ViewModels.Basics;
using CM.WinRT.Demos.ViewModels.Other;
using CM.WinRT.Demos.ViewModels.Win8;
using Caliburn.Micro;
using Windows.UI.Xaml.Controls;

namespace CM.WinRT.Demos.ViewModels
{
    public class MainViewModel : ScreenWithNavigation
    {
        public MainViewModel(INavigationService navigation): base(navigation)
        {
            PopulateDemoList();
        }

        public BindableCollection<DemoGroup> Demos { get; set; }

        public void LaunchDemo(ItemClickEventArgs args)
        {
            Navigation.NavigateToViewModel((args.ClickedItem as Demo).ViewModelType);
        }

        private void PopulateDemoList()
        {
            Demos = new BindableCollection<DemoGroup>
                        {
                            new DemoGroup { Title = "Caliburn.Micro Basics", 
                                            Demos = new BindableCollection<Demo>
                                                        {
                                                            new Demo{ Title = "Basic Binding", Subtitle = "Let's explore the basics of convention-based binding", ViewModelType = typeof (SimpleBindingDemoViewModel) },
                                                            new Demo{ Title = "Actions", Subtitle = "Use actions instead of Commands", ViewModelType = typeof (ActionsViewModel) },
                                                            new Demo{ Title = "Screens and Conductors", Subtitle = "Composibility and Screen lifecycle", ViewModelType = typeof (ScreenConductorViewModel) },
                                                            new Demo{ Title = "Event Aggregation", Subtitle = "Communicating between ViewModels", ViewModelType = typeof (EventAggregationViewModel) },
                                                            new Demo{ Title = "Coroutines", Subtitle = "Asynchrony before async - and it's still awesome", ViewModelType = typeof (CoroutinesViewModel) },
                                                        }
                                },
                            new DemoGroup { Title = "Working with Windows 8", 
                                            Demos = new BindableCollection<Demo>
                                                        {
                                                            new Demo{ Title = "Settings", Subtitle = "Create a settings page for your app", ViewModelType = typeof (SettingsViewModel) },
                                                            new Demo{ Title = "Sharing", Subtitle = "Implement the sharing contract", ViewModelType = typeof (SharingViewModel) },
                                                            new Demo{ Title = "Searching", Subtitle = "Tie in to Win8's universal search contract", ViewModelType = typeof (SearchViewModel) },
                                                            new Demo{ Title = "File Pickers", Subtitle = "Use the Windows 8 File Chooser", ViewModelType = typeof (FilePickerViewModel) },
                                                            //new Demo{ Title = "Saving State", Subtitle = "Understanding the app lifecycle and preparing for it", ViewModelType = typeof (SimpleBindingDemoViewModel) },
                                                        }
                                },
                            new DemoGroup { Title = "Other View Concerns", 
                                            Demos = new BindableCollection<Demo>
                                                        {
                                                            new Demo{ Title = "View States", Subtitle = "Using the ViewStateManager from your ViewModels", ViewModelType = typeof (ViewStateViewModel) },
                                                            new Demo{ Title = "Message Boxes", Subtitle = "How to do Message Boxes in Win8/Caliburn.Micro", ViewModelType = typeof (MessageBoxViewModel) },
                                                            new Demo{ Title = "Orientation & Snapping", Subtitle = "Responding to orientation changes and snapping", ViewModelType = typeof (SnappingViewModel) },
                                                            new Demo{ Title = "Flyouts", Subtitle = "Showing popups in WinRT called Flyouts", ViewModelType = typeof (FlyoutViewModel) },
                                                            new Demo{ Title = "App Bars", Subtitle = "Using App Bars", ViewModelType = typeof (AppBarsViewModel) },
                                                        }
                                },
                            new DemoGroup { Title = "Working Around Annoyances", 
                                            Demos = new BindableCollection<Demo>
                                                        {
                                                            new Demo{ Title = "No PropertyChanged UpdateSourceTrigger", Subtitle = "How to get instant change notifications from text boxes", ViewModelType = typeof (UpdateSourceTriggerViewModel) },
                                                            new Demo{ Title = "No String Format in Binding", Subtitle = "Using ValueConverters to do what WinRT XAML should do for you", ViewModelType = typeof (FormatViewModel) },
                                                            new Demo{ Title = "No Validation in Binding", Subtitle = "Seriously?  No validation in binding.  Sigh.", ViewModelType = typeof (ValidationViewModel) },
                                                        }
                                }
                        };            
        }
    }
}
