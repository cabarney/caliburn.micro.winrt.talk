﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras;
using Caliburn.Micro.WinRT.Extras.Results;

namespace CM.WinRT.Demos.ViewModels.Annoyances
{
    public class UpdateSourceTriggerViewModel : ScreenWithNavigation
    {
        private string _name1;
        private string _name2;

        public UpdateSourceTriggerViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "No UpdateSourceTrigger=Propertychanged  :(";
        }

        public string Name1
        {
            get { return _name1; }
            set
            {
                if (value == _name1) return;
                _name1 = value;
                NotifyOfPropertyChange(() => Name1);
                NotifyOfPropertyChange(() => CanSayHello1);
            }
        }

        public string Name2
        {
            get { return _name2; }
            set
            {
                if (value == _name2) return;
                _name2 = value;
                NotifyOfPropertyChange(() => Name2);
                NotifyOfPropertyChange(() => CanSayHello2);
            }
        }

        public void SayHello1()
        {
            Run.Coroutine(new MessageDialogResult("Hello from Calibur.Micro!", "Hello " + Name1));
        }

        public void SayHello2()
        {
            Run.Coroutine(new MessageDialogResult("Hello from Calibur.Micro!", "Hello " + Name2));
        }

        public bool CanSayHello1 { get { return !string.IsNullOrEmpty(Name1); } }
        public bool CanSayHello2 { get { return !string.IsNullOrEmpty(Name2); } }

    }
}
