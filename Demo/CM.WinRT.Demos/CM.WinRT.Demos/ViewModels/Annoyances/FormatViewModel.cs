﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Annoyances
{
    public class FormatViewModel : ScreenWithNavigation
    {
        private DateTime _rightNow;

        public FormatViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "No String.Format in Binding :(";
            RightNow = DateTime.Now;
        }

        public DateTime RightNow
        {
            get { return _rightNow; }
            set
            {
                if (value.Equals(_rightNow)) return;
                _rightNow = value;
                NotifyOfPropertyChange(() => RightNow);
            }
        }
    }
}
