﻿using System;
using System.Collections;
using System.ComponentModel;
using Caliburn.Micro;

namespace CM.WinRT.Demos.ViewModels.Annoyances
{
    public class ValidationViewModel:ScreenWithNavigation, INotifyDataErrorInfo
    {
        private string _input;
        private bool _inputIsInvalid;

        public ValidationViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "No Validation in Binding Statements?  Really?";
        }

        public string Input
        {
            get { return _input; }
            set
            {
                if (value == _input) return;
                _input = value;
                NotifyOfPropertyChange(() => Input);
                NotifyOfPropertyChange(() => InputErrorMessage);
                NotifyOfPropertyChange(() => InputIsInvalid);
            }
        }

        public bool InputIsInvalid
        {
            get { return !string.IsNullOrEmpty(InputErrorMessage); }
        }

        public string InputErrorMessage { get
        {
            var en = GetErrors("Input").GetEnumerator();
            en.MoveNext();
            return en.Current == null ? "" : en.Current.ToString();
        }}


        public IEnumerable GetErrors(string propertyName)
        {
            if(propertyName == "Input")
            {
                if (!string.IsNullOrEmpty(Input) && string.Compare(Input, "I hate this") != 0)
                    yield return "You have to hate this.";
            }
        }

        public bool HasErrors { get; private set; }
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
    }
}
