﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras.Messages;

namespace CM.WinRT.Demos.ViewModels.Win8
{
    public class FilePickerViewModel : ScreenWithNavigation, IHandle<FileOpenPickerResponse>
    {
        private readonly IEventAggregator _events;
        private string _fileNames;

        public FilePickerViewModel(INavigationService navigation, IEventAggregator events) : base(navigation)
        {
            DisplayName = "File Pickers";
            _events = events;
            _events.Subscribe(this);
        }

        public string FileNames
        {
            get { return _fileNames; }
            set
            {
                if (value == _fileNames) return;
                _fileNames = value;
                NotifyOfPropertyChange(() => FileNames);
            }
        }

        public void ChooseFile()
        {
            _events.Publish(new FileOpenPickerRequest
                                {
                                    CommitButtonText = "Open File",
                                    MultipleFilesAllowed = false,
                                    FileTypeFilter = new List<string>{".jpg", ".png", ".gif", ".bmp"}
                                });
        }

        public void ChooseMultipleFiles()
        {
             _events.Publish(new FileOpenPickerRequest
                                {
                                    CommitButtonText = "Open File",
                                    MultipleFilesAllowed = true,
                                    FileTypeFilter = new List<string>{".jpg", ".png", ".gif", ".bmp"}
                                });           
        }

        public void Handle(FileOpenPickerResponse message)
        {
            if (message.Files == null || message.Files.Count == 0)
                return;

            FileNames = string.Join(", ", message.Files.Select(x => x.DisplayName));
        }
    }
}
