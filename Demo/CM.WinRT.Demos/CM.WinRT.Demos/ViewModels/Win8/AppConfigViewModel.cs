using Caliburn.Micro;
using Windows.Storage;

namespace CM.WinRT.Demos.ViewModels.Win8
{
    public class AppConfigViewModel : Screen
    {
        private string _userName;

        protected override void OnActivate()
        {
            base.OnActivate();
            UserName = (string)ApplicationData.Current.LocalSettings.Values["UserName"];
        }

        protected override void OnDeactivate(bool close)
        {
            ApplicationData.Current.LocalSettings.Values["UserName"] = UserName;
            base.OnDeactivate(close);
        }
        
        public string UserName
        {
            get { return _userName; }
            set
            {
                if (value == _userName) return;
                _userName = value;
                NotifyOfPropertyChange(() => UserName);
            }
        }
    }
}