﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Windows.UI.ApplicationSettings;

namespace CM.WinRT.Demos.ViewModels.Win8
{
    public class SettingsViewModel : ScreenWithNavigation
    {
        public SettingsViewModel(INavigationService navigation) : base(navigation)
        {
            DisplayName = "Working with Settings in WinRT";
        }

        public void OpenSettings()
        {
            SettingsPane.Show();
        }
    }
}
