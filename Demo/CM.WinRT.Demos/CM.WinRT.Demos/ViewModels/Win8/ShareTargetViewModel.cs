﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras.Results;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;

namespace CM.WinRT.Demos.ViewModels.Win8
{
    public class ShareTargetViewModel : Screen
    {
        private string _sharedInformation;

        public ShareTargetViewModel()
        {
            DisplayName = "You want to share with me?";
        }

        protected override async void OnViewReady(object view)
        {
            base.OnViewReady(view);

            SharedInformation = await Parameter.ShareOperation.Data.GetTextAsync();
        }

        public string SharedInformation
        {
            get { return _sharedInformation; }
            set
            {
                if (value == _sharedInformation) return;
                _sharedInformation = value;
                NotifyOfPropertyChange(() => SharedInformation);
            }
        }

        public IEnumerable<IResult> Share()
        {
            Parameter.ShareOperation.ReportStarted();
            yield return new MessageDialogResult("You have shared the follwing information: " + SharedInformation, "Thank You");
            TryClose();
            Parameter.ShareOperation.ReportCompleted();
        }

        public ShareTargetActivatedEventArgs Parameter { get; set; }
    }
}
