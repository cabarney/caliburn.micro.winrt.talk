﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras;

namespace CM.WinRT.Demos.ViewModels.Win8
{
    public class SearchViewModel : ScreenWithNavigation, ISupportSearching
    {

        public SearchViewModel(INavigationService navigation)
            : base(navigation)
        {
            DisplayName = "Searching";
        }

        private List<string> _results;
        public List<string> Results
        {
            get { return _results; }
            set
            {
                if (Equals(value, _results)) return;
                _results = value;
                NotifyOfPropertyChange(() => Results);
            }
        }

        protected override void OnViewReady(object view)
        {
            OnQuerySubmitted(Parameter);
        }

        public string Parameter { get; set; }
        
        public void OnQuerySubmitted(string queryText)
        {
            if (!IsActive)
                Navigation.NavigateToViewModel<SearchViewModel>(queryText);
            if (string.IsNullOrEmpty(queryText))
                return;
            Results = _options.Where(x => x.ToLower().Contains(queryText.ToLower())).ToList();
        }

        public IEnumerable<string> OnQuerySuggestionsRequested(string queryText)
        {
            if (string.IsNullOrEmpty(queryText))
                return new string[]{};

            return _options.Where(x => x.ToLower().Contains(queryText.ToLower())).ToList();
        }

        public IEnumerable<SearchResultSuggestion> OnResultSuggestionsRequested(string queryText)
        {
            return null;
        }

        public void OnResultSelected(string tag)
        {
            
        }

        private readonly string[] _options = new string[]
                                        {
                                            "Alabama",
                                            "Alaska",
                                            "Arizona",
                                            "Arkansas",
                                            "California",
                                            "Colorado",
                                            "Connecticut",
                                            "Delaware",
                                            "Florida",
                                            "Georgia",
                                            "Hawaii",
                                            "Idaho",
                                            "Illinois",
                                            "Indiana",
                                            "Iowa",
                                            "Kansas",
                                            "Kentucky",
                                            "Louisiana",
                                            "Maine",
                                            "Maryland",
                                            "Massachusetts",
                                            "Michigan",
                                            "Minnesota",
                                            "Mississippi",
                                            "Missouri",
                                            "Montana",
                                            "Nebraska",
                                            "Nevada",
                                            "New Hampshire",
                                            "New Jersey",
                                            "New Mexico	",
                                            "New York",
                                            "North Carolina",
                                            "North Dakota",
                                            "Ohio",
                                            "Oklahoma",
                                            "Oregon",
                                            "Pennsylvania",
                                            "Rhode Island",
                                            "South Carolina",
                                            "South Dakota",
                                            "Tennessee",
                                            "Texas",
                                            "Utah",
                                            "Vermont",
                                            "Virginia",
                                            "Washington",
                                            "West Virginia",
                                            "Wisconsin",
                                            "Wyoming"
                                        };
    }
}
