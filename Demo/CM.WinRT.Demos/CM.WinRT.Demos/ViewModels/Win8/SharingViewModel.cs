﻿using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras;
using Windows.ApplicationModel.DataTransfer;

namespace CM.WinRT.Demos.ViewModels.Win8
{
    public class SharingViewModel : ScreenWithNavigation, ISupportSharing
    {
        private string _shararableText;
        private string _selectedText;

        public SharingViewModel(INavigationService navigation) : base(navigation)
        {
            ShararableText = "Caliburn.Micro is a small, yet powerful framework, designed for building applications across all Xaml Platforms. With strong support for MVVM and other proven UI patterns, Caliburn.Micro will enable you to build your solution quickly, without the need to sacrifice code quality or testability.";
        }

        public string ShararableText
        {
            get { return _shararableText; }
            set
            {
                if (value == _shararableText) return;
                _shararableText = value;
                NotifyOfPropertyChange(() => ShararableText);
            }
        }

        public string SelectedText
        {
            get { return _selectedText; }
            set
            {
                if (value == _selectedText) return;
                _selectedText = value;
                NotifyOfPropertyChange(() => SelectedText);
            }
        }

        public void OnShareRequested(DataRequest dataRequest)
        {
            dataRequest.Data.Properties.Title = "Sharing Text with Caliburn.Micro";
            dataRequest.Data.SetText(SelectedText);
            
            //dataRequest.Data.SetUri(new Uri("http://caliburnmicro.codeplex.com"));
        }
    }
}
