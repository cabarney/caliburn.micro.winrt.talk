﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CM.WinRT.Demos.ViewModels;
using CM.WinRT.Demos.ViewModels.Win8;
using CM.WinRT.Demos.Views;
using CM.WinRT.Demos.Views.Win8;
using Caliburn.Micro;
using Caliburn.Micro.WinRT.Extras;
using FirstFloor.XamlSpy;
using Windows.UI.Xaml;


namespace CM.WinRT.Demos
{
    sealed partial class App:CaliburnApplication
    {
        private FirstFloor.XamlSpy.XamlSpyService _xamlSpyService;

        public App()
        {
            InitializeComponent();

            _xamlSpyService = new XamlSpyService(this);
        }

        private WinRTContainer _container;

        protected override void Configure()
        {
            base.Configure();

            _container = new WinRTContainer(RootFrame);
            _container.RegisterWinRTServices();

            _container.RegisterInstance(typeof(CallistoSettingsService), null, new CallistoSettingsService());
            _container.RegisterInstance(typeof(ShareSourceService), null, new ShareSourceService(RootFrame));
            _container.RegisterInstance(typeof(SearchService), null, new SearchService(RootFrame, typeof(SearchViewModel)));

            _container.SettingsCommand<AppConfigViewModel>(1, "My Application Configuration");
            
            _container.RegisterSingleton(typeof(PickerController), null, typeof(PickerController));
            var pickerController = _container.GetInstance(typeof (PickerController), null) as PickerController;
            pickerController.Start();

            _container.RegisterInstance(typeof(IFlyoutHelper), null, new FlyoutHelper());
            

            ConventionManager.AddElementConvention<UpdatingTextBox>(UpdatingTextBox.BindableTextProperty, "Text", "TextChanged");
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override Type GetDefaultView()
        {
            return typeof (MainView);
        }

        protected async override void OnSearchActivated(Windows.ApplicationModel.Activation.SearchActivatedEventArgs args)
        {
            base.OnSearchActivated(args);
            Initialise();
            RootFrame.Navigate(typeof(SearchView), args.QueryText);
            
            Window.Current.Content = RootFrame;
            Window.Current.Activate();
        }

        protected override void OnShareTargetActivated(Windows.ApplicationModel.Activation.ShareTargetActivatedEventArgs args)
        {
            base.OnShareTargetActivated(args);
            Initialise();
            RootFrame.Navigate(typeof(ShareTargetView), args);
            
            Window.Current.Content = RootFrame;
            Window.Current.Activate();
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            base.OnWindowCreated(args);

            //_xamlSpyService.StartService();
        }
    }
}