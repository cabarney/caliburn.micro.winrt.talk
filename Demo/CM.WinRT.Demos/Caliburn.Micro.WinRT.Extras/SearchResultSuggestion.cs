using Windows.Storage.Streams;

namespace Caliburn.Micro.WinRT.Extras
{
    public class SearchResultSuggestion
    {
        public string Text { get; set; }
        public string DetailText { get; set; }
        public string Tag { get; set; }
        public IRandomAccessStreamReference Image { get; set; }
        public string ImageAlternateText { get; set; }
    }
}