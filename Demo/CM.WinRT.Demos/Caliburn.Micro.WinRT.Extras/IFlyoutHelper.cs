using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace Caliburn.Micro.WinRT.Extras
{
    public interface IFlyoutHelper
    {
        void ShowDialog<T>(PlacementMode placement, UIElement placementTarget, Action<T> onInitialize = null,
                           Action<T> onClose = null) where T : Screen;
    }
}