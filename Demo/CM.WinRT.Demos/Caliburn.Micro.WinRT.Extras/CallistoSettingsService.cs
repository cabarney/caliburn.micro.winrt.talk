using System.Linq;
using Callisto.Controls;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;

namespace Caliburn.Micro.WinRT.Extras
{
    public class CallistoSettingsService
    {
        private SettingsPane _settingsPane;

        public CallistoSettingsService()
        {
            _settingsPane = SettingsPane.GetForCurrentView();
            _settingsPane.CommandsRequested += OnCommandsRequested;
        }

        protected virtual void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            var caliburnCommands = Enumerable.Cast<CallistoSettingsCommand>(IoC.GetAllInstances(typeof (CallistoSettingsCommand)));
            var settingsCommands = caliburnCommands.Select(c => new SettingsCommand(c.CommandId, c.Label, h => OnCommandSelected(c)));

            settingsCommands.Apply(args.Request.ApplicationCommands.Add);
        }

        protected virtual void OnCommandSelected(CallistoSettingsCommand command)
        {
            var view = ViewLocator.LocateForModelType(command.ViewModelType, null, null) as FrameworkElement;

            if (view == null)
                return;

            var viewModel = ViewModelLocator.LocateForView(view);

            if (viewModel == null)
                return;

            ViewModelBinder.Bind(viewModel, view, null);

            var settingsFlyout = new SettingsFlyout
            {
                FlyoutWidth = command.Width,
                HeaderText = command.Label,
                Content = view,
                IsOpen = true
            };

            settingsFlyout.Closed += (s, e) =>
            {
                var deactivator = viewModel as IDeactivate;

                if (deactivator != null)
                {
                    deactivator.Deactivate(true);
                }
            };

            var activator = viewModel as IActivate;

            if (activator != null)
            {
                activator.Activate();
            }
        }
    }
}
