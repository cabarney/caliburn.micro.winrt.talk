using Windows.ApplicationModel.DataTransfer;

namespace Caliburn.Micro.WinRT.Extras
{
    public interface ISupportSharing
    {
        void OnShareRequested(DataRequest dataRequest);
    }
}
