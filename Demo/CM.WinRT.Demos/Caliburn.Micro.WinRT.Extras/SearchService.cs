using System;
using Windows.ApplicationModel.Search;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Caliburn.Micro.WinRT.Extras
{
    public class SearchService
    {
        private readonly Frame _rootFrame;
        private readonly Type _defaultSearchViewModelType;
        private readonly SearchPane _searchPane;

        public SearchService(Frame rootFrame, Type defaultSearchViewModelType)
        {
            _rootFrame = rootFrame;
            _defaultSearchViewModelType = defaultSearchViewModelType;
            _searchPane = SearchPane.GetForCurrentView();

            _searchPane.QuerySubmitted += OnQuerySubmitted;
            _searchPane.SuggestionsRequested += OnSuggestionsRequested;
            _searchPane.ResultSuggestionChosen += OnResultSuggestionChosen;
        }

        void OnResultSuggestionChosen(SearchPane sender, SearchPaneResultSuggestionChosenEventArgs args)
        {
            var supportSearching = GetSearchableViewModel();
            if(supportSearching == null)
                return;
            
            supportSearching.OnResultSelected(args.Tag);
        }

        protected virtual void OnQuerySubmitted(SearchPane sender, SearchPaneQuerySubmittedEventArgs args)
        {
            var supportSearching = GetSearchableViewModel();
            if(supportSearching == null)
                return;

            supportSearching.OnQuerySubmitted(args.QueryText);
        }

        protected virtual void OnSuggestionsRequested(SearchPane sender, SearchPaneSuggestionsRequestedEventArgs args)
        {
            var supportSearching = GetSearchableViewModel();
            if(supportSearching == null)
                return;

            var querySuggestions = supportSearching.OnQuerySuggestionsRequested(args.QueryText);
            args.Request.SearchSuggestionCollection.AppendQuerySuggestions(querySuggestions);

            var resultSuggestions = supportSearching.OnResultSuggestionsRequested(args.QueryText);
            if(resultSuggestions != null)
                foreach (var s in resultSuggestions)
                    args.Request.SearchSuggestionCollection.AppendResultSuggestion(s.Text, s.DetailText, s.Tag, s.Image, s.ImageAlternateText);
        }

        private ISupportSearching GetSearchableViewModel()
        {
            var view = _rootFrame.Content as FrameworkElement;

            if (view == null)
                return null;

            var supportSearching = view.DataContext as ISupportSearching;

            if (supportSearching != null)
                return supportSearching;
            if (_defaultSearchViewModelType != null)
                supportSearching = IoC.GetInstance(_defaultSearchViewModelType, "") as ISupportSearching;
            return supportSearching;
        }
    }
}