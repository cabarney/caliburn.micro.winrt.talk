using System;
using System.Collections.Generic;
using Caliburn.Micro.WinRT.Extras.Messages;
using Windows.Storage;
using Windows.Storage.Pickers;

namespace Caliburn.Micro.WinRT.Extras
{
    public class PickerController : IHandle<FileOpenPickerRequest>, IHandle<FileSavePickerRequest>, IHandle<FolderPickerRequest>
    {
        private readonly IEventAggregator _events;

        public PickerController(IEventAggregator events)
        {
            _events = events;
        }

        public void Start()
        {
            _events.Subscribe(this);
        }

        public async void Handle(FileOpenPickerRequest message)
        {
            var fp = new FileOpenPicker();
            foreach(var t in message.FileTypeFilter)
                fp.FileTypeFilter.Add(t);
            
            fp.CommitButtonText = message.CommitButtonText;

            var response = new FileOpenPickerResponse {Tag = message.Tag, Files=new List<StorageFile>()};
            if (message.MultipleFilesAllowed)
            {
                var files = await fp.PickMultipleFilesAsync();
                response.Files.AddRange(files);
            }
            else
            {
                var file = await fp.PickSingleFileAsync();
                response.Files.Add(file);
            }
            _events.Publish(response);
        }

        public async void Handle(FileSavePickerRequest message)
        {
            var fp = new FileSavePicker
                         {
                             CommitButtonText = message.CommitButtonText,
                             DefaultFileExtension = message.DefaultExtension,
                             SuggestedStartLocation = message.SuggestedStartLocation,
                             SuggestedSaveFile = message.SuggestedSaveFile
                         };
            var response = new FileSavePickerResponse {Tag = message.Tag, File = await fp.PickSaveFileAsync()};

            _events.Publish(response);
        }


        public async void Handle(FolderPickerRequest message)
        {
            var fp = new FolderPicker();
            fp.CommitButtonText = message.CommitButtonText;
            foreach(var filter in message.FileTypeFilter)
                fp.FileTypeFilter.Add(filter);
            fp.SuggestedStartLocation = message.SuggestedStartLocation;
            var response = new FolderPickerResponse
                               {
                                   Tag = message.Tag,
                                   Location = await fp.PickSingleFolderAsync()
                               };
            _events.Publish(response);
        }
    }
}
