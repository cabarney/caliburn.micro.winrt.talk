﻿using System;
using Callisto.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace Caliburn.Micro.WinRT.Extras
{
    public class FlyoutHelper : IFlyoutHelper
    {
        #region IFlyoutHelper Members

        public void ShowDialog<T>(PlacementMode placement, UIElement placementTarget, Action<T> onInitialize = null,
                                  Action<T> onClose = null) where T : Screen
        {
            var vm = IoC.Get<T>();
            UIElement view = ViewLocator.LocateForModel(vm, null, null);

            ViewModelBinder.Bind(vm, view, null);

            if (onInitialize != null)
                onInitialize(vm);

            ScreenExtensions.TryActivate(vm);

            var f = new Flyout
                        {
                            Content = view,
                            Placement = placement,
                            PlacementTarget = placementTarget,
                            IsOpen = true,
                        };

            if (onClose != null)
                f.Closed += (sender, o) => onClose(vm);
        }

        #endregion
    }
}