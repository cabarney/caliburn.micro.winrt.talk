using Windows.Storage;

namespace Caliburn.Micro.WinRT.Extras.Messages
{
    public class FileSavePickerResponse
    {
        public string Tag { get; set; }
        public StorageFile File { get; set; }
    }
}