using System.Collections.Generic;

namespace Caliburn.Micro.WinRT.Extras.Messages
{
    public class FileOpenPickerRequest
    {
        public List<string> FileTypeFilter { get; set; }
        public string CommitButtonText { get; set; }
        public bool MultipleFilesAllowed { get; set; }
        public string Tag { get; set; }
    }
}