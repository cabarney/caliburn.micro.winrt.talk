using System.Collections.Generic;
using Windows.Storage.Pickers;

namespace Caliburn.Micro.WinRT.Extras.Messages
{
    public class FolderPickerRequest
    {
        public string Tag { get; set; }
        public string CommitButtonText { get; set; }
        public List<string> FileTypeFilter { get; set; }
        public PickerLocationId SuggestedStartLocation { get; set; }
    }
}