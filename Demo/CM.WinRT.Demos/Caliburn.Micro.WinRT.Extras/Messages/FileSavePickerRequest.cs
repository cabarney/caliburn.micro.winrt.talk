using Windows.Storage;
using Windows.Storage.Pickers;

namespace Caliburn.Micro.WinRT.Extras.Messages
{
    public class FileSavePickerRequest
    {
        public string DefaultExtension { get; set; }
        public string SuggestedFileName { get; set; }
        public PickerLocationId SuggestedStartLocation { get; set; }
        public StorageFile SuggestedSaveFile { get; set; }
        public string CommitButtonText { get; set; }
        public string Tag { get; set; }
    }
}