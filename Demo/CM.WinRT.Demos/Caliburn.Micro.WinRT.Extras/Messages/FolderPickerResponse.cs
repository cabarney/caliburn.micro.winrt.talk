using Windows.Storage;

namespace Caliburn.Micro.WinRT.Extras.Messages
{
    public class FolderPickerResponse
    {
        public string Tag { get; set; }
        public StorageFolder Location { get; set; }
    }
}