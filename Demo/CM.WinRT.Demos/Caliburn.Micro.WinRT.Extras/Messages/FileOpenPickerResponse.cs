using System.Collections.Generic;
using Windows.Storage;

namespace Caliburn.Micro.WinRT.Extras.Messages
{
    public class FileOpenPickerResponse
    {
        public string Tag { get; set; }
        public List<StorageFile> Files { get; set; }
    }
}