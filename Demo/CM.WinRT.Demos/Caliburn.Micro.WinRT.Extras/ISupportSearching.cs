using System.Collections.Generic;

namespace Caliburn.Micro.WinRT.Extras
{
    public interface ISupportSearching
    {
        void OnQuerySubmitted(string queryText);
        IEnumerable<string> OnQuerySuggestionsRequested(string queryText);
        IEnumerable<SearchResultSuggestion> OnResultSuggestionsRequested(string queryText);
        void OnResultSelected(string tag);
    }
}