using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Caliburn.Micro.WinRT.Extras.Results
{
    public class ControlVisualStateResult : VisualStateResult
    {
        private readonly string _controlName;


        public ControlVisualStateResult(string stateName, string controlName, bool useTransitions = true) : base(stateName, useTransitions)
        {
            _controlName = controlName;
        }

        public string ControlName
        {
            get { return _controlName; }
        }

        private Control FindChild(Control parent, string name)
        {
            return parent.FindName(name) as Control;
        }

        public override void Execute(ActionExecutionContext context)
        {
            if (!(context.View is Control))
                throw new InvalidOperationException("View must be a Control to use VisualStateResult");

            var view = (Control)context.View;

            var control = FindChild(view, _controlName);

            VisualStateManager.GoToState(control, StateName, UseTransitions);

            OnCompleted();
        }
    }
}