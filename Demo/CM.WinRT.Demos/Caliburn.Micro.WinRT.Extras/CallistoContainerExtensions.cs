using Callisto.Controls;

namespace Caliburn.Micro.WinRT.Extras
{
    public static class CallistoContainerExtensions
    {
        public static void SettingsCommand<TViewModel>(this SimpleContainer simpleContainer, object commandId, string label, SettingsFlyout.SettingsFlyoutWidth width = SettingsFlyout.SettingsFlyoutWidth.Narrow)
        {
            simpleContainer.Instance(new CallistoSettingsCommand(commandId, label, typeof (TViewModel))
            {
                Width = width
            });
        }
    }
}
