using System;
using Callisto.Controls;

namespace Caliburn.Micro.WinRT.Extras
{
    public class CallistoSettingsCommand
    {
        private readonly object _commandId;
        private readonly string _label;
        private readonly Type _viewModelType;

        public CallistoSettingsCommand(object commandId, string label, Type viewModelType)
        {
            _commandId = commandId;
            _label = label;
            _viewModelType = viewModelType;

            Width = SettingsFlyout.SettingsFlyoutWidth.Narrow;
        }

        public Type ViewModelType
        {
            get
            {
                return _viewModelType;
            }
        }

        public object CommandId
        {
            get
            {
                return _commandId;
            }
        }

        public string Label
        {
            get
            {
                return _label;
            }
        }

        public SettingsFlyout.SettingsFlyoutWidth Width
        {
            get; set;
        }
    }
}
